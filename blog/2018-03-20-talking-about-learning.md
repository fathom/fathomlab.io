---
title: Talking about learning
date: 2018-03-20
author: Jared Pereira
---

I recently gave a talk at EthCC about [mathetics and decentralized
systems](https://www.youtube.com/watch?v=1Bk4jB4p4rM&t). 

## A quick summary
_Mathetics_ is the science of learning; mathetics is to learning as pedagogy is
to teaching. It shifts the focus of education to how individuals create
knowledge, not how authorities organize and pass it down. 

I think it's an especially important word in the context of the this new
community around blockchains and decentralized systems for two main reasons:

Firstly, people who work/live/think/hang-out in this field are learning _all the
time_. Cryptocurrencies and blockchains have been many people's introduction to
the world's of economics, game-theory and countless others. The field is
constantly surfacing new connections and ideas, and progressing rapidly. We need
to think about how we operate mathetically; how our culture, products, and
communnity supports learning.

Secondly, on a more abstract level, these ideas around decentralized systems
 and **social computation** are actually changing _how people learn_. They act
as lenses and frameworks for individuals to interpret the world and themselves.
This is a powerful tool and something that deserves to be thoughtfully and
carefully explored. As with anything that has the potential to be pervasive,
the psychological and mathetic impact of this technology deserves thoughtful and
careful consideration.

I think learning is not something we talk about enough, in almost all contexts.
We don't think deeply about the long-term learning implications of our products
nor our development cycles. Or the sustainability of our research paradigms.
There are so many areas to explore here. It's essential that we open up a
conversation. 

## So let's talk!
I brought up a lot of these points in my talk, but I realized after that there
wasn't any real concrete step forward that I was presenting. To remedy that, I'm
proposing a...

### Mathetics Meetup!

A tri-weekly hourly gathering to talk about learning!

The goal is to have a starting point for the conversation, a space for people
interested in mathetics to raise and explore ideas, thoughts, and concerns. The
plan is to leave it open-ended and see what the community needs, wants and can
do. 

#### Structure
Each meeting will have a predefined topic and follow a rough timeline: 

1. 20min presentation around the topic
2. 30min discussion around the topic/presentation OR open-floor period
3. 10min to organize the next meeting and any next steps.

Next steps could be reaching out to people, collaborating on some writing,
finding and sharing information, anything. These meetings are the starting point
not the ends.

All meetings will be on Jisti and livestreamed and recorded. In addition,
summaries and transcripts will be published.

### The First Meeting
The first meeting will occur in 2 weeks in the first week of April. In order to
coordinate the times we'll use a [doodle
poll](https://doodle.com/poll/qkn8zfk5cwc4bdu4). **If you want to attend please
answer the poll**.

We'll coordinate this, and other future fathom events in [this
calender](https://calendar.google.com/calendar/embed?src=consensys.net_4sjoim52o36u7omeupju81dgfs%40group.calendar.google.com&ctz=Asia%2FDubai)
Here you can find the date and time (when it's finalized) and the link to join
the video call.

For a topic we'll be talking about **Talking to kids about blockchains**!

## Join us!
If learning is interesting to you please join! 

If you're keen to discuss something, have someone you'd love to have talk, or
are just excited to share your thoughts on this whole thing please reach out.
You can email us [here](mailto:contact@fathom.network) or find me on
[twitter](https://twitter.com/jrdprr). I'm excited to see where this goes!

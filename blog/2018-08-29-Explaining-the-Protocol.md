---
title: A Closer Look at the Protocol
date: 2018-12-19
author: Julius
---

_This post presents two additional ways of understanding our protocol, which are
at opposite ends of the complexity and accessibility. A state machine and a
comic strip. We hope some might find this helpful and if not, give us feedback
to help us improve them._

As our team grew over the last months, we had to explain our protocol a few
times and realized that only our resource to defer to was the
[whitepaper]('../static/pdf/whitepaper.pdf') which, while being technically
accurate, is not the most accessible or condensed. This was especially true
for the many ways in which an assessment can go awry, such as assessors failing
to complete a certain stage or not showing up in the first place.

Also, our implementation of the protocol on the ethereum blockchain introduced
certain extra-steps, which are not specified in the whitepaper,
but will affect the user experience in our app [^1].

So we created two other resources: 
- 1. Some simple diagrams, which eventually evolved into a comic strip
  (that might someday be readily translated into other languages).
- 1. A [state machine](/static/img/AssessmentStateMachine.png): A flow-chart like graph, which lays out all possible states an assessment can be in and what actions make it transition to the next. This is especially to highlight the ways assessments can fail.

Let's have some fun first and look at the diagrams! 

This first one gives a general overview how an assessment goes down: 

![](/static/img/processComic.png?raw=true) 

The other two are to explain what happens afterwards. Here's how we find the final score:

![](/static/img/finalScore.gif?raw=true)

But what happens if there is a draw? Or if there is no agreement at all? We
could just answer in text *or* take it as an excuse to make some more diagrams:

![](/static/img/finalScoreEdgeCases.png?raw=true)

Next, we have a diagram that explains how an assessors position inside or
outside the winning cluster and their relation to the other assessors affects
their payout:

![](/static/img/payment3Graphs.png?raw=true)

The second diagram explains the same thing but condenses the 3 steps from above into one:
![](/static/img/payment1Graph.png?raw=true)

Are these making sense to you? We'd love to hear what you think! Feel free to
leave a comment in this [issue](https://gitlab.com/fathom/docs/issues/9), in our
[riot-chat](https://riot.im/app/#/room/#fathom:matrix.org) or by sending us an
email.

If you still have questions (and a faible for state
machines), then check whether [this](/static/img/AssessmentStateMachine.png)
(somewhat intimidating) diagram answers them for you.

[^1]: This is because every state change on the ethereum blockchain has some
    costs associated to it, the so-called _gas-costs_. [Gas](https://ethereum.stackexchange.com/questions/3/what-is-meant-by-the-term-gas) can be thought of as
    the fuel of the blockchain. Every state change consumes at least a little
    bit of it and without it, one can not only read from the chain, but not
    write to it. Among other this is to prevent trolls from spamming (image
    every spam-mail would cost a cent!) and to pay the people who keep the chain
    running (aka the 'miners') by processing the incoming transactions. That is
    why every internal state change of our contracts need to be triggered from
    the outside and we can not automatically cancel timed-out assessment or give
    out refunds.

function parse_yaml {
    local prefix=$2
    local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
    sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
        awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

root=$(pwd)

# Building the drafts index pages
( cd _drafts
  rm index.md

  for file in *.md; do
      [ -e "$file" ] || continue
      ( eval $(parse_yaml "$file")
        echo "- [$title](/_drafts/`basename "$file" .md `.html)" >> index.md
      )
  done
)

function buildBlog() {
    depth=$1
    parents=$2

    for file in $(ls *.md | grep -v 'index.md' | sort -r); do
        [ -e "$file" ] || continue
        (eval $(parse_yaml "$file")
         if [ "$draft" != "true" ]; then
             for (( c=1; c<=depth - 1; c++)); do
                 echo -n '\t' >> $root/blog/index.md
             done
             echo -n "- [$title](/blog/$parents/`basename "$file" .md `.html) _($date)_ \n" >> $root/blog/index.md
         fi
        )
    done


    for folder in $(find ./* -maxdepth 0 -type d -not -path '*/\.*'); do
        if ! ((depth == 2)); then
            echo "\n----------------\n" >> $root/blog/index.md
        fi
        for (( c=1; c<=depth - 1; c++)); do
            echo -n '\t' >> $root/blog/index.md
        done

        if [ ! -f index.md ]; then
            echo -n "- [${folder:2}](/blog/${parents:1}${folder:1}/) \n" >> $root/blog/index.md
        else
            echo -n "- ${folder:2} \n" >> $root/blog/index.md
        fi
        (cd "$folder"; buildBlog $((++depth)) $parents${folder:1})
    done
}

# Building the blog index page
( cd blog
  rm index.md

  buildBlog 1 ""

  CONTENT=$( cat <<EOF
---
title: "the fathom blog:"
---
EOF
         )

  echo -e "$CONTENT \n \n$(cat index.md)" > index.md
)

rm -rf public/

# converting markdown to html and preserving dir structure
for folder in $(find . -type d -not -path '*/\.*'); do
    (cd $folder
     for file in `find . -maxdepth 1 -name "*.md" -not -name "README.md"`; do
         [ -e "$file" ] || continue
         mkdir -p "$root/public/${folder#./}"

         pandoc -f markdown -t html  "$file" -o "$root/public/${folder#./}/`basename "$file" .md`.html" -B "$root/includes/nav.html" -H "$root/includes/header.html" --css "/static/style.css"  2> /dev/null
     done)
done

( cd public
mkdir -p .well-known/acme-challenge
cd .well-known/acme-challenge
echo -e d1wNmEMhMNU8EqoSNNh8HbnXiUnkQnhaYLAgVPk_LpI.7fgMLAkmjQj92bXGL3W7fbCfsuRCM9fmhEyxK-nt9NE > d1wNmEMhMNU8EqoSNNh8HbnXiUnkQnhaYLAgVPk_LpI
)

cp -r static public

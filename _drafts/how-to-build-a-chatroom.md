---
title: How to build a chatroom
date: 2017-12-02
---

This will be a walkthrough on how to think about and create chatrooms. 

## What's the point of a chatroom?
A chatroom is a social environment based around on-chain data. The idea is to create meaningful spaces for interaction by first limiting participants to identities which meet certain criteria, and then to allow for complex and meaningful interactions by integrating smart-contract functions and events.

## What are the components of a chatroom?
A chatroom is defined by a smart contract which has two main components. 

1. A function called *getPermissions* which takes an address and returns a string of bytes defining the permissions of that address, what they're allowed to do in a chatroom.
2. A IPFS hash corresponding to a JSON object that contains metadata, such as a description of the chatroom, or the ABIs and addresses of contracts it references.

Let's start look at a reall simple example first:

## The Universal Room
```{.javascript}
pragma solidity ^0.4.16;

contract universalRoom {
    string public metadata;

    function universalRoom() public {
        metadata = 'QmYXQL4zvaKKXSCwhYw9CVkAbyfUxiVXmJTDXoQjqf3HkF';
    }

    function getPermissions(address) static public returns (bytes32) {
        return (0x1);
    }
}
```

First in the constructor we're setting the metadata string to a set IPFS hash. Which corresponds to this JSON object:

```{.json}
{
	"name": "Universal Room",
	"description": "This room has a permissions function that 
                    always returns true. Everyone is welcome!"
}
```

Then we're defining a funciton that takes an address and always returns 0x1.

### Where's this used?
Let's say we deploy this contract at `0x7612dc5dbfeec008c91f93339e792efc7b0cbf7a`
When a user arriaves at this [chatroom](https://darq.chat/?0x7612dc5dbfeec008c91f93339e792efc7b0cbf7a) the app gets the metadata hash from the contract and fetches the description and name.
Then, when they log in, they're asked to sign a message with their metamask account. This signed data is then sent to the server which extracts their public address from it. 

The server then feeds that address into the `getPermissions` function on the smart contract for the chatroom their trying to sign into.

If that function returns 31 '0's followed by a '1' it authenticates that user and allows them into the chat room. Else it doesn't.

## Adding on complexity
This contract doesn't really do much, it just let's everybody in.

Let's instead a create a chatroom that only let's someone in if they have a certain amount of tokens. Token controlled chat rooms!

```{.javascript}

pragma solidity ^0.4.16;
import 'Token.sol'

contract TokenRooms {
    string public metadata;

    function MetaTokenRoom(string _metadata) public {
        metadata = _metadata;
    }

    function getPermissions(address user, address _token,
                            uint _threshold) 
                            constant public returns(bytes32){
        if(Token(_token).balanceOf(user) >= _threshold) {
            return (0x1);
        }
        else {
            return (0x0);
        }
    }
}
```

So we have a little more code here, but the structure remains the same, there's still a metadata string and a getPermissions function that returns 0x1 upon certain conditions.

### Meta Rooms
If you look closer at that function it takes 2 extra arguments beyond just the address of the user. This introduces the concept of meta-rooms. These are contracts that do not define a single chatroom, but many chatrooms that follow the same set of rules, and just differ on parameters.
Any chatroom interacting with this contract would now be referenced by not only the address of the contract but by those extra arguments as well.

'getPermissions' is checking if the user (the given address) has a balance greater than `_threshold` in a token at address `_token_`. Every possible value for each of those two variables would define a different social environment (aka chatroom), pertaining to different tokens and different amounts one has to hold.

If a user wanted to access a room they'd have to pass these arguments in the URL. For example let's look at this long-ass URL: https://darq.chat/?0xac15e35744bbe35ffa6d4fab6139d0475da8e25a/0x9abf1d0766e43c95031b3fc0357da38e5f01af6d/1

The first parameter after the '?' is the address of the metacontract: `0xac15e35744bbe35ffa6d4fab6139d0475da8e25a`. 
The second parameter is the address of the token in question: `0x9abf1d0766e43c95031b3fc0357da38e5f01af6d`
The last parameter is the amount of tokens a user needs to be in the room: `1`

So overall this URL corresponds to a room where a user needs 1 token from a ERC20 token deployed at `0x9abf1d0766e43c95031b3fc0357da38e5f01af6d`

### More Metadata
The metadata for this room also looks a bit differnt.

```{.json}

{
    "name": "Meta Token Room",
    "description": "You need {{_threshold}} tokens from {{_token}}",
    "abi": [...],
    "contracts":
    [{
        "name": "token",
        "address": "{{_token}}",
        "abi": [...]
    }]
}

```
The `abi` property is new. It defines the ABI for the chatroom contract itself. This is neccesary because we don't know how many arguments a meta-room might take, or of what type.

More interesting is the `contracts` property. This defines an array of objects, each of which pertains to a contract that a user in this chatroom can access for various purposes. For example, if the user wants to transfer tokens they could call `/token transfer ...` or if they ever need to refer to the event they could with `token Transfer`. This is all sourced from the ABI supplied.

#### Templates
You by this point probably notice the strings surrounded by {{ curly braces }} corresponding to the two extra arguments we were dealing with earlier. These are template variables that get substituted with the values specific to the room. 

The user would be greeted with a description that substities in the threshold and address the room is dealing with, but more importantly when they call `/token` commands, they will interface with the token contract they're concerned with!


# Keeping it simple
Both of these are fairly minimal examples, but they're already getting a but large and cumbersome. Hopefully by focusing on just a few moving pieces, the getPermissions function and templating, and keeping everything else fairly static, these chatrooms will be easy to understand and create. 

Layering things on top of each other is where the functionality gets really fun. What if you create a contract that has a `kick(address _user)` function that anyone with permissions can call. Then the metadata makes that contract available to use in the chatroom itself, and now you have rudimentary governance built in.

There's a huge space of possibilities here. We really just want to get a small set of tools with valuable intersections to start playing around. 

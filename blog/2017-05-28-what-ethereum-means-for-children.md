---
title: What Ethereum Means for Children
date: 2017-05-28
author: Jared Pereira
---
My main focus with fathom is on changing the structure and function of social educational systems using Ethereum and decentralized technologies, but there is a interesting _pedagogical_ perspective on how this new paradigm can change education. 

The data and tools available as a result of smart-contract and blockchain based systems make possible a unique learning environment. This may sound mundane, but when taking into account the scale at which blockchain technology seems to want to pervade our society, it has some pretty large implications.

### Open-Source Government 
Take for example civics, a pretty valuable addition to any education system. It's a fairly hot topic in the space right now on how we can actually do decentralized governance, so let's put that to the side and instead enter a hypothetical where our current political systems are exactly the same, just running (and scaling) on Ethereum.

Now imagine a classroom in this world and in it a teacher (yes there are still teachers here) introducing the government, and its different branches and formations. They probably begin simply enough, perhaps with a history lesson or relevant anecdote, and probably from there go on into the broad strokes. Perhaps its a democracy, a futarchy, a benevolent dictatorship, I don't really want to get into the details. 

The first important part is this: The moment an enterprising kid asks "wait but how why what when" the teacher can say, **"do you want to see the source code?**".

They can pull it up and start highlighting the small pieces that are immediately comprehensible, maybe just permissions and voting, and the students can search through for where these pieces touch their life. They could even look up the identities of high-profile politicians and see what they've been up to, the commands they've been executing and what they did. The teacher could guide them through, or just let them run free. 

Let that go on for a while, and then the truly fun part can begin. The teacher asks, **"Do you want to try it out?"**

With a few commands you have a class playing in a sandboxed world government, arguing over who gets to be Hegemon.

## Computation + Constructionism for _everything_ 
Because the blockchain and smart-contracts, and decentralized social systems in general, are so broadly applicable this essentially sets the stage for the child-computer interface to burst into social systems. 

> _It is about an end to the culture that makes science and technology alien to the vast majority of people. Many cultural barriers impede children from making scientific knowledge their own. Among these barriers the most visible are the physically brutal effects of deprivation and isolation. Other barriers are more political. Many children who grow up in our cities are surrounded by the artifacts of science but have good reason to see them as belonging to "the others"; in many cases they are perceived as belonging to the social enemy._ <br>- Seymour Papert, Mindstorms

With the Ethereum World Computer in the hands of children, it can be about an end to the culture that makes _society_ alien to the vast majority of people. Students can poke and prod and _hack_ the systems that so deeply influence our lives. They can even create and extend them, pulling them from the distance into their own plane. Imagine kids writing smart-contracts for the governance of some shared resources, i.e like Recess but instead of [King Bob](http://recess.wikia.com/wiki/King_Bob) governing the playground commons, we'd have code, collectively written and experimented with, maybe forked from the Mayor's office. 

I think Ethereum is going to make for a fantastic playground. Imagine all the future authors seeing how their work moves throughout a publishing house, or the entrepreneurs pulling the git repos for their favorite companies. The previously incomprehensible, gargantuan, foreign systems and organizations that influence lives could be compiled to _code_, code that's open, accessible and usable; pokable and prodable.

### The future
This isn't a new dream, its there in the open-source ethos and in Papert's vision of computational education, but the extent to which Ethereum and blockchain will pervade society elevates the dream to a new heights. Imagine the empowerment one would feel when you have the world government at your finger tips, not to control, but to use and extend and engage with.

We're bringing this radical empowerment to the next generation. I cannot wait to see what they do.

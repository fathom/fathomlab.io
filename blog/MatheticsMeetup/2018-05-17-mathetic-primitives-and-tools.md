---
title: 'Meetup 2: Mathetic Primitives and Tools'
date: 2018-5-23
---

Round two! (you can see the followup to Round 1 [here](./2018-03-28-1-talking-to-children.html#outcomes))

## News!

We've set up a Matrix channel to house this community. We chose matrix because
of it's awesome federated features (allowing ownership where neccessary) as well
as the ability to have rooms as a part of multiple communities. It's also just a
really cool community and has the awesome riot client.

You can find the room on riot
[here](https://riot.im/app/#/room/%23mathetics-meetup:matrix.org).

## Details:

- **When**: 8PM UTC, May 30th
- **Where:** [This Jitsi video chat](https://meet.jit.si/WorriedFruitShootSelfishly)
- **What**: Mathetic Primitives and Tools (and how we can use them)

## Topic:

[Alexander Singh](https://twitter.com/automaticyes) will be giving a talk
interspered with discussions exploring 3 different Mathetic Primitives and
examples of each. 

We've used that phrase a bunch so it might be useful to to try and define it. A
Mathetic primitive is any tool, mental model, or idea, that furthers learning
that cannot be decomposed into a smaller tool mental model or idea.

These ideas are abstract so we want to anchor these primitives to examples and
then anchor _those_ examples to concrete applications that we as a community can
use to learn about mathetics.

## Goal:

We want to start building a better framework to think about mathetics. This is a
discussion that a lot of people get very excited about but the broad range of
experiences in relation to education and learning make it difficult to have a
deep and effective conversation. This meeting is starting to build a shared
language. 

These ideas will be initially messy, but we want to start talking about them in
order to move forward :) 

### Material

Nada, but hopefully we'll be able to build nice tools for creating and sharing
collections of material in the future.

### Outcomes

- **Presentation**: https://www.are.na/alex-singh/mathetics-primitives-presentation
- **Recording**: https://youtu.be/BbY0Pg-bC8Q _(we only got the first half of the
presentation sadly)_

We had a pretty awesome turnout, with a surprising number of educators present.

Alex's presentation centered on questions and networks, giving both some
definitions and concrete examples.

#### Measurement and Primitives
One of the main topics of discussion centered around measurement in relation to
these primitives. This came from two main perspectives: 

1. Individuals questioning themselves, creating questions to drive their own
   learning
2. Systems questioning individuals in order to measure their learning. 

The language of formative and summative assessments came up and we had some
awesome insights into formal assessment process presented.

#### Our own mathetic tools

One of the main takeaways from the presentation was that we could use these
primitives for our own learning about mathetics. To that end we've created a
couple spaces. 

The first is an [arena
channel](https://www.are.na/jared-pereira/mathetics-meetup). For now this is
holding Alex's presentation and some earlier research but it will be the home
for ideas, concepts and explorations related to the ideas of mathetics. It's a
space anyone can drop links and thoughts, and we can progressivley make some
sense of it all. 

The second is a [github organization](https://github.com/mathetics-meetup). This
is a space for our more strutured information, for any endeavors we may want to
explore (like essays or software)


### Moving Forward

This meetup was in my eyes an awesome step forward from the first one. We had a
great presentation from Alex and some fantastic discussion that drew from a set
of diverse backgrounds and experiences. 

One way we can make these meetups even better is by creating better artifacts
from them. This could be better notes from the discussion, or more writing or
essays or other media produced.

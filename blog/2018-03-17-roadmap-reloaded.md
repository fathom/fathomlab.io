---
title: Roadmap Reloaded
date: 2018-03-17
---

_Update (04/20): We now describe our roadmap using GitLab's Milestone feature.
Check out the new masterplan [here](https://gitlab.com/groups/fathom/-/milestones/4)._


# Roadmap Reloaded
After some weeks of intense discussions & little coding, we're back are ready to
share our plan for getting the fathom network off the ground.

## The Playground
This starts with the playground. We've breifly mentioned this distributed
learning program before, but work is in progress specifying it fully (see this [pull
request](https://gitlab.com/fathom/fathom.gitlab.io/merge_requests/31)),
explaining what it is, what it will be centered on, and how the players are
expected to interact.

We want to launch the Playground around July. In order to do so there are
serveral things that need to be in place. 

## Infrastructure
We need infrastructure in order for the playground participants to interact and
communicate, and create.

We originally starting building a smart-contract permissioned chatroom in order
to enable this, but have since been abstracting the concept further. What we
have arrived at is in two parts: 
1. a collection of small tools and libraries to enable rich social interaction
   around on-chain data and identities
2. a web-application to explore fathom concepts and run the transactional part
   of assessments
   
### The Tools
#### Bridger
The first tool we are going to build is Bridger, a library and web-app to allow
anyone to easily make and verify claims about Ethereum identities and data
on-chain. The goal is to form a bridge between things going on on-chain (like
fathom assessments) to existing platforms (like Github), in a simple, secure and
easily extensible way.

This will let us use these platforms as a communication substrate and start the
playground, allowing us to iterate and experiment quickly.

#### Communication Systems
Of course though, we aren't satisfied with depending on Web 2.0 platforms so in
parralel we will be exploring ways to build small communication systems. This
may look like the smart-contract enabled chatrooms we've been working with so
far, _or_ integration into systems like
[orbit](https://github.com/orbitdb/orbit-db) or
[ssb](https://github.com/ssbc/secure-scuttlebutt)

### The App
The main fathom application is used for exploring concepts, starting
assessments, and conducting them. The plan is to keep it very minimal, only
dealing with the parts that touch our smart-contracts and rely on links via
Bridger to conduct the actual social part of assessments.

## Launching the Playground
Once Bridger and the Fathom App are ready, it comes time to launch the
Playground. The first part of this is opening up applications. We're still
working on the application process, but at least part of it will be tied to an
on-chain process to distribute a stipend to playground participants.

### Playing!
Once the playground is in progress we will continue with admissions through a
rolling process. We'll be supporting the learners, and learning ourselves,
creating credentials and tools. 

##  Onboarding and Organization
We are happy to share that our team has grown again: Welcome aboard Antoine
Estienne! 

We've opened an [org-repo](https://gitlab.com/fathom/org), to hold our
organizational practices, including a new contribution guide, a more
detailed roadmap, and issue and MR templates. 

# Moving forward
We're still figuring things out and exploring this space. However, we have a
clear path forward that we are diligently working towards. 

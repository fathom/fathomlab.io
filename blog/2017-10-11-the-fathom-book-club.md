---
title: The Fathom Bookclub
date: 2017-10-11
---

A lot of the ideas we're dealing with in fathom aren't really knew, even putting aside the ones clearly stemming from the rest of the blockchain space. Our thoughts on educational systems, and societal structures, all have been explored in the past, in a ton of interesting ways and with great depth. 

We want to stay abreast of all the work whirling around, and, more than just consume, engage with the ideas and concepts surrounding what we're trying to do. 

To that end, this book club should help provide a framework for that exploration and engagement. 

## What're we going to read
The chief focus of fathom is on the self-directed education of individuals, and how they use that education to interact with each other and so readings around these topics make a lot of sense. 

However we also want to explore ideas important to the crypto space we are in, general economics and crypto, as well as any other things that come up. We're leaving it open-ended essentially.

## How we're going to organize it
For every book an issue is created describing why we're reading it and some initial thoughts/direction. Anyone is welcome to contribute to the discussion. Other issues can also be opened to dive deeper into specific questions or ideas. 

During the reading period (1-3 weeks) thoughts are collected from the relevant issues and a merge request is created, adding a post to the blog. This post will aim to collect the general thoughts and sentiments of those who participated in the process. After the reading period has ended this post will then be published, and the next book announced.

## Book 1: Deschooling Society by Ivan Illich
This book, which two of the fathom team have started reading already (Jared and Julius), is crazy _before_ you take into account when it was written. Once you do however it becomes a tad ridiculous. The ideas of course hold true, but so do so  many of the criticisims that Illich hoped would be addressable. 

What's even more fascinating is the role he see's technology playing in society, and how he descibes how networks can easily oppress and create huge negative externalities. We're going to have a lot to work through for this one. 

It's a relatively short collection of essays essentially and so a lot of points are oft repeated. But each piece can mostly stand on it's own. 

You can find a html version of the document [here](http://www.davidtinapple.com/illich/1970_deschooling.html).

## Timeline:
We'll try for a week. See how it goes. You can find the issue [here](https://gitlab.com/fathom/fathom.gitlab.io/issues/12). Keep an eye on that issue for additional thoughts and eventually the merge-request.

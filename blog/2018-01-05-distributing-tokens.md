---
title: Token Distribution Mechanism
date: 2018-01-10
author: Jared Pereira
---

We need some mechanism to initially distribute our new tokens to the
participants of the fathom network. The classic way to do this is through a
token sale, simultaneously raising funds for a network and distributing tokens
to those who value them.

However, it is difficult to accurately price tokens (so that they go to those who
value them _most_) and it is harder to ensure that those who maximally value the
token will be most in line with the long term goals of the network, especially
because they may have speculative goals.

It is also potentially dangerous to immediately distribute all the tokens in an
economy in a "drop" event. It makes the network less responsive as it grows,
cutting off valuable opportunities for regulating token mechanics.

### Our solution
We want a continous distribution mechanism that allocates _newly minted_ tokens through a
randomized process, weighted by participation in the network.

A good metric for participation is the amount of tokens a user has spent or risked in
assessments. These are staked schelling game where current participants who
have credentials validate those who want to prove they do. If you want to learn
more about it you can read [our whitepaper](../../whitepaper/2-Protocol.html#sec:assprocess)

In order to distribute the tokens we hold a lottery every Y blocks (an epoch)
for X tokens. At the start of each epoch a target hash is set as the
hash of the first block. In the next Y blocks any user can submit a "bid", a
hash of:

 1. Their address
 2. The address of an assessment they partipated in, to completion, within the epoch.
 3. An integer <= the amount of tokens they spent or risked in that assessment.
 4. The salt of that assessment (explained below).
 
All these values are fixed for a given assessment, but an individual can vary
the value of the integer they submit. This gives them a potential hash for every
token they use in an assessment.

#### A random value
The salt is to ensure that bids are not deterministic when an assessor decides
to accept the assessment, and is derived hasing and then XOR-ing all the salts
assessors submit in the commit-reveal phase.

The winning hash is the one closest to the target for that period and the
winner gets X tokens.

The current closest hash is stored in the contract during an epoch, so that an
individual only has to submit their hash if they know it beats it. Eventually
this can be taken care of automatically for users by a script that listens for
completed assessments. 

You can view the first implementation of the contracts
[here](https://gitlab.com/fathom/assess/merge_requests/94).

Attacks and Considerations
------

The main line of attack is control of either the target hash or one's bid hash.

The former is easily influenced by miners but because it is set before any bids
are allowed, it isn't meaningful to exert control over it.

For the latter an individual knows their address, the address of the assessment,
and the amount of tokens they are risking, _before_ they accept the assessment.
However they cannot compute the salt and so cannot control the hash.

However because this is a commit and reveal scheme, the _last_ assessor to
commit can exert some control by decieding whether or not to reveal. In this
case though if they choose not to reveal, they lose their stake and are no
longer assessors for that assessment and hence cannot submit any hashes based on
it, nor will their salt influence the final one.

### Openness

One shortcoming of this approach is that an individual needs to have tokens in
order to earn tokens. We don't see a way out of this without relying
on external, arbitrary mechanisms.


Tuning Parameters
---------

Ideally the network should aim to have a reasonable amount of inflation, taking
into account tokens burned during the assessment process. This is to incentivize
and allow for spending over holding, and to drive activity in the network.

To achieve this the parameters of the systetm will likely need to be tuned as it
runs. Initially the token reward will not matter while the epoch length will be
key, but as the system grows this will reverse.

The ultimate goal of all of this is to grow the community and create scalable
infrastructure for people to interact with and around. It's not likely we'll get
it right on the first try, but we think this is a good starting point.

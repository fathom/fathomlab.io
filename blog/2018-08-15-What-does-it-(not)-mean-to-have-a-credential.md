---
title: What does it (not) mean to have a credential?
date: 2018-08-15
author: Julius Faber
---

__This post is the first of a series where I want to explain in what ways
fathom-credentials are meaningful, even though the individuals involved in
creating them have no reputation, how this can lead to them not being "true"
and why that is not a problem (but a feature). In this post, I'll start by
asking a related, very basic question about regular, institution-backed
credentials.__

Let's start at the beginning: What is a credential?

In a nutshell, a credential is saying 

 **Y says X knows Z.**

![Figure 1: Y says X knows Z](../static/img/YsaysXknowsZ.png)

Let's look more closely at the two parts of that definition:

### What does it mean that *X knows Z*?

It does sound a bit obvious for the moment, but we will want to come back to
this at a later point, when we talk about defining certificates:

In a very general manner, "knowing something" defines a relation to a piece of
knowledge. It means being able to recite it, apply it, explain it to someone
else and, at last, to extend its boundaries.

And once it is more or less defined what it means to know something, e.g. to "know
react-redux", this definition starts being useful to those subscribing to
it: They can talk about their problems and goals related to it and
coordinate(!) and communicate on a common basis in order to solve their problems.

That was the general part of the definition. Easy. Next!

### What does it mean that *Y says...*?

This is the more interesting bit.

Essentially, Y vouches that someone X fulfills the definition __to know Z__. Y
can be many different entities depending on the kind of attestation: 
- An individual e.g. a bailer/guarantor, 
- an organization or company which issues a letter of reference or a testimonial,
- a representative of an organization, such as a doctor handing out a prescription
- an institution: Diplomas!, or
- a government: Document of identification

For each of these cases the essential question to ask here, is...

### Why should you trust what Y says?

Let's look at how one can arrive at the conclusion that X does indeed know Z. 

In a very general manner, this process will have two parts:

1) the methodology (questionnaire, interview, examination,...) used to test X  
2) the person(s) administering it.

Importantly, the second point is the crux! No matter how ingenious the process,
the person administering the process can always deviate from it or falsify it.

Of course, this is not a new problem. We have come up with complicated
interwoven systems of processes and responsibilities, where the ones executing
them control and report to each other so that the system's dependency on the
integrity, diligence and accuracy of individuals is reduced to a minimum:
Bureaucracies, yay!

But bureaucracies are slow. This is okay for some scenarios (passing laws or
judgments, changing constitutions,...) but not for others. Mostly those, where
services have to be customized for each or most individuals, such as health care
services, socials services and education.

In the 21st century more so than ever, individuals have very diverse histories,
backgrounds, upbringings and perspectives that, and that's a personal belief of
mine, uniquely qualifies them to be good at certain things and that merit being
supported and attested to.

But this is not supposed to become a rant against our current education system,
so I'll put aside my arguments why it is stupid to attempt providing the __same
education__ to everybody and get back to the point that it is not even feasible:

For any process that is to be applied at a large scale, it is impossible to
verify whether it REALLY was executed the same for everybody.
For example: One can not verify whether a multiple choice test or a
reading-skill test was fairly administered to all 6-8 year old's of an entire
city, district or school.
Nor will a university get a lot of students or funding because its exams
are especially fair.

Consequently, and this is close to the final point of this post, as nobody can
verify how well a large institution functions, there is no alternative to
**relying on reputation**: 


### Implications of Relying on Reputation

Using our first definition this would mean: I assume that **X knows Z, because Y
has a good reputation**.

Unfortunately, there is a major drawback to this.

If all certificates are valid only because of Y's reputation, all certificates are
become connected. A few bad apples can spoil the bunch.

For institutions, this means that to stay reputable, they have to be
conservative and favor false-negatives over false-positives (meaning if in
doubt: reject/fail the applicant!). If they weren't, a few falsely issued would
boomerang back to all others and invalidate them.

For individuals, this has important consequences and I am sure everyone could
tell a bunch of stories to back up my first of the two summarizing statements of this post:

**1. Failing an institution does not mean you don't know something.**

**2.Institutional certificates depend on reputation** because processes are not verifiable at scale*.

__*If you are a blockchain-familiar reader, you might have disagreed with my last
sentence. Or, smiling sheepishly, have added an "..until now" in your head. I
agree with you. And in the next post, I will look at how fathom credentials,
created by an on-chain process, will change who will be able to have what part
of their skill set being attested to and why anybody should believe them.__








---
title: "Book Club 1: Deschooling Society"
date: 2017-11-24
author: Jared Pereira
---
Having read [Deschooling
Society](http://www.davidtinapple.com/illich/1970_deschooling.html) what's most
interesting to me about Illich's work is that he isn't solely concerned with
schools, but uses them as a lense to explore the nature of all institutions
in society.

> _Universal education through schooling is not feasible. It would be no more
> feasible if it were attempted by means of alternative institutions built on
> the style of present schools. Neither new attitudes of teachers toward their
> pupils nor the proliferation of educational hardware or software (in classroom
> or bedroom), nor finally the attempt to expand the pedagogue's responsibility
> until it engulfs his pupils' lifetimes will deliver universal education. The
> current search for new educational funnels must be reversed into the search
> for their institutional inverse: educational webs which heighten the
> opportunity for each one to transform each moment of his living into one of
> learning, sharing, and caring._ <br>- Ivan Illich, Deschooling Society

He was a prescient critic. Though he wrote this in 1970, universal education
continues to be unfeasible and our society continues to pursue it relentlessly.

This to a certain extent mirrored the development of my thoughts. I originally
was focused on building peer-to-peer learning systems to address what I saw as a
defeciency in the education system. That was an ignition point for me to explore
decentralized systems, and my scope expanded to thinking about society and how
it uses trust and institutions.

Blockchains were of course integral to this mental journey. They too provide a
paradigm to explore the nature of institutions in society, but, crucially, also
provide tools to effect change, on a societal scale.

This isn't to say that Illich was just a doomsayer, he proposed solutions along
with his concerns, exploring alternative structures, and building out what
educational webs could actually look like. The question is, why weren't these
ideas just as prescient??

## Learning Webs
I, Jared, was pretty fascinated by those solutions and systems Illich proposed.
They ranged from the practical to the absolutely wild and, though I didn't agree
with all of them, they were always fascinating.
[Here](./2017-11-24-deschooling-through-the-blockchain.html)
I explore a little bit how these ideas relate to blockchain technology and why
they didn't come to fruition.

## A First Amendment for Education

Illich blew Julius' mind with the radicality of his analysis and thought
provoking ideas, the most radical of which he picks up
[here](./2017-11-24-singleton-for-certificates.html), in this
post about how to break up that powerful and life-determining institution that
schools have become today.

## A review & discussion

For those of you who want to dig deeper and further engage with us on these
ideas, we published a summary of Illich's first chapter [here](./deschoolingChap1Review.html). We plan to do some stuff on Genius, an annotations platform, as well so let us know if you're interested in that.





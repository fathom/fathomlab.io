---
title: 'fathom: a universal system of assessment'
--- 

fathom is a decentralized protocol for creating and assessing credentials
through the consensus of knowledge communities.

The protocol enables communities to form around the concepts they care about and
create definitions of what it means to know them. It incentivizes members to use
these definitions to accurately assess those looking to gain credentials and
join the community.

**This creates a universal credential ecosystem, where individuals are free to
learn as they please and collaborate with anyone in the world.**

<div class="links">
[whitepaper](https://docs.fathom.network/en/latest/) |
[newsletter](https://tinyletter.com/FathomNetwork) |
[code](https://gitlab.com/fathom) | [riot chat](https://riot.im/app/#/group/+fathom:matrix.org)
</div>
---

## How it works:

1. You offer tokens to start an assessment with the number of assessors you
   desire.
2. Assessors can accept by staking tokens.
3. You collaborate with each assessor to carry out the assessment, and each
   submits a score.
4. Each score is measured against the overall consensus and is rewarded or
   punished accordingly. Your final score is the average of the consensus.

## A more wide-eyed explanation:

fathom is fundamentally about expanding how we can communicate useful
experiences. Society functions on knowing what individuals can do based on what
they have done; it's how we hire, allocate funds, and even make friends.

In communities with small populations, people are able to form deep
understandings of the skills and identities of other members in their group by
word of mouth, apprenticeships, and other social connections. While this method
results in comprehensive and accurate interpretations of people’s skills, it
cannot scale to large populations.

In large scale communities, emphasis is placed on standardized testing, usually
provided by central educational institutions. While this method of measuring
skills and worth is efficient, it is inflexible and overlooks many relevant and
valuable experiences that are not part of the institution’s system. As a result,
individuals often choose to accrue only experiences that can be broadly
communicated by the system. In college, a Math major is less likely to enroll in
an art class because his experience in art is neither considered valuable to the
institution granting him a degree, nor easily conveyed to a potential employer.

This is holding our society back. If individuals were free to build their
experiences tailored to their unique aims, **and were able to communicate those
experiences reliably to any entity in the world**, there would be an order of
magnitude shift in the efficacy of social organization at every scale.

### How fathom fixes things

Fathom aims to enable this freedom by focusing on the social process that
generates the credentials themselves. By creating incentives for communities to
form and support themselves in the assessment process, you have a system for
creating and organizing microcredentials that are both meaningful and scalable.

You can distribute the work and trust required to perform those assessments,
allowing for scalability, along with adding value, not only for the individual
aiming for a credential, but those assessing and the community as a whole.

Because the protocol focuses only on how individuals interact, and maps that as
closely as possible to how credentials themselves influence how people interact,
fathom is capable of capturing any credential communities need it to.

---
## Why Ethereum

fathom is built and deployed as a smart-contract system on Ethereum. This allows
the protocol to function without any central point of trust, failure, or
control. This is important in the context of credentials for multiple reasons.

### Self-Sovereign Identity
In fathom, credentials form identity, and individuals have a right to control
their identity. It should not be able to be destroyed, forged, or otherwise
compromised, by any power. Blockchains, and Ethereum in particular, form an
incredible foundation for self-sovereign identity. Ethereum provides a robust
and versatile paradigm for identity management for lower level qualities, such
as immutability and public-key based accounts, and higher level constructs, such
as proxy contracts and others.

### Protocol trust and incentives
Because individuals need to trust in the process that generates the credentials
themselves, Ethereum is necessary to ensure security. For that to happen
individuals need to trust that the protocol can't be abused and that it
functions exactly as described. This is a coding problem. Obviously our
smart-contracts have to be robust. Ethereum guarantees that robustness, once
deployed, will execute. This is especially important when dealing with fathom’s
economic incentives.

### Forming a foundation
Above all , fathom is built on Ethereum in order to form the foundation for our
future decentralized social systems. Anyone in the world can build something
using fathom credentials, and everyone else can trust what they build. The
systems they build can be planetary scale social systems, a new digital society.
